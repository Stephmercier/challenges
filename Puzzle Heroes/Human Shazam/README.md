﻿# Puzzle Heroes

## Human Shazam

Un mystérieux medley ce trouve dans ce répertoire, vous devez trouver d'où sont tirées chacune des pièces le configurant.

Écrire les réponses dans ce README!

#### Réponses:

###### Extrait 1
votre réponse: Some temple in legend of Zelda -> legend of zelda (ocarina of time ou Majora's mask?)

###### Extrait 2
votre réponse: Fox

###### Extrait 3
votre réponse: Assassin's creed

###### Extrait 4
votre réponse: Oh my god Oh my god (OK)

###### Extrait 5
votre réponse: Futurama  (OK)

###### Extrait 6
votre réponse: Duck tales the moon

###### Extrait 7
votre réponse: Death note 

###### Extrait 8
votre réponse: Conker's bad hair day (Big myghty poo)

###### Extrait 9
votre réponse: ?

###### Extrait 10
votre réponse: Legend of zelda (Hyrule theme)

Bonne chance !