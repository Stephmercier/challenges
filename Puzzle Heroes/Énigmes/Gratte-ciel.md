﻿# Puzzle Heroes

## Gratte-Ciel

### Mise en situation
On a un Gratte-Ciel de 300 étages. On a trois melons d'eau de même consistance et de même densité. Le but est de trouver le premier étage à partir duquel, lorsqu'on le lance par la fenêtre, un melon d'eau va éclater en tombant sur le sol. Lorsqu'un melon ne casse pas, on peut aller le chercher et le réutiliser. Si le melon n'éclate pas en tombant, il n'est aucunement endommagé. Lorsqu'un melon éclate, on ne peut pas le réutiliser. Le melon a autant de chance d'éclater en tombant de l'étage 1 que de l'étage 300, mais les trois melons vont toujours éclater en tombant à partir du même étage pour le même bâtiment.

### Instructions
Pour réussir cette énigme, vous devez répondre aux deux questions suivantes dans ce README.

* Quelle stratégie serait à employer afin de minimiser le nombre de fois qu'on lance un melon d'eau par la fenêtre afin de trouver le premier étage à partir duquel les melons éclatent?
* Quel est le nombre minimum de lancer de melons pour trouver cet étage?

N'oubliez pas que vous avez seulement trois melons et qu'il y a 300 étages!

### Indice
Commencez par trouver une stratégie qui fonctionne avec moins de melons sur un bâtiment avec moins d'étages. Appliquez le même raisonnement pour un problème avec 3 melons pour 300 étages.

### Réponse
Écrivez votre réponse ici:
Je lancerais un premier melon aux étages par bond de 20 (maximum 15 lancés)
Quand le melon éclate, je lance un melon par bond de 5 dans la tranche de 20 étages contenant l'étage où le melon éclate, donc au maximum 4 lancés
	(s'il éclate à 20, je fais des bonds de 5 de 0 à 19)
	(s'il éclate à 25, je fais des bonds de 5 de 20 à 24)
Finalement, s'il le faut, je parcours au maximum 4 autres étages avec le dernier melon en partant du plus bas.

Cela donne, en pire cas, 21 lancés ;)